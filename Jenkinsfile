#!groovy

podTemplate(
  containers:[ 
    containerTemplate(name: 'jdk11',                image:'adoptopenjdk:11-jdk-openj9',   ttyEnabled:true, command:'cat'),
    containerTemplate(name: 'docker',               image:'docker:18',                    ttyEnabled:true, command:'cat') 
  ],
  volumes: [
    hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
  ])
{
  node(POD_LABEL) {
  
    stage ('checkout') {
      checkout_details = checkout scm
      do_k8s_update = false
    }
  
    stage ('build service assembly') {
      container('jdk11') {
        dir ('service') {
          sh './gradlew --no-daemon -x test -x integrationTest --console=plain clean build'
        }
      }
    }
  
    // https://www.jenkins.io/doc/book/pipeline/docker/
    stage('Build Docker Image') {
      container('docker') {
        //sh 'ls service/build/libs'
        docker_image = docker.build("lsp_core_service")
      }
    }
  
  
  
    // This build produces an executable war file of the form lsp_core-1.0.0-SNAPSHOT.war 
    // Bundle it as a docker container
    stage('Publish Docker Image') {
      container('docker') {

        if ( checkout_details?.GIT_BRANCH == 'master' ) {
          String latest_tag = null;
          def props = readProperties file: './service/gradle.properties'
          println("Got props: asString:${props} appVersion:${props.appVersion}/${props['appVersion']}");
          constructed_tag = "build-${props?.appVersion}-${checkout_details?.GIT_COMMIT?.take(12)}"
          String[] semantic_version_components = props.appVersion.toString().split('\\.')

          println("Considering build tag : ${constructed_tag} version:${props.appVersion}");
          // Some interesting stuff here https://github.com/jenkinsci/pipeline-examples/pull/83/files

          if ( ! (props?.appVersion?.contains('SNAPSHOT')) ) {
            do_k8s_update=true
            docker.withRegistry('http://docker.semweb.co', 'semweb-nexus') {
              println("Publishing released version with latest tag and semver ${semantic_version_components}");
              docker_image.push('latest')
              docker_image.push(props.appVersion)
              docker_image.push("${semantic_version_components[0]}.${semantic_version_components[1]}".toString())
              docker_image.push(semantic_version_components[0])
            }
          }
          else {
            docker.withRegistry('http://docker.semweb.co', 'semweb-nexus') {
              println("Publishing snapshot-latest");
              docker_image.push('snapshot-latest')
            }
          }
        }
        else {
          println("Not publishing docker image for branch ${checkout_details?.GIT_BRANCH}. Please merge to master for a docker image build");
        }
      }
    }


    stage('Rolling Update') {
      if ( do_k8s_update ) {
        kubernetesDeploy(
          // Credentials for k8s to run the required deployment commands
          kubeconfigId: 'kubeconfig2',
          // Definition of the deployment
          configs: 'deployment.yml',
          // Where to pull the image from
          dockerCredentials: [
            [ url:'http://docker.semweb.co', credentialsId:'semweb-nexus' ]
          ]
        )
      }
      else {
        println("Skip k8s deploy - only deploying released artefacts from master branch");
      }
    }
  }

}
