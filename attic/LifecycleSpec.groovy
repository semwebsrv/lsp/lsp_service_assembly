package lspcore


import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import java.security.Key;
import java.security.PublicKey
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import spock.lang.Shared
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import groovy.util.logging.Slf4j
import spock.lang.Stepwise
import com.hazelcast.config.Config
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.map.IMap
import java.util.concurrent.TimeUnit
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer
import static groovyx.net.http.HttpBuilder.configure
import java.io.InputStreamReader
import lspcore.readinglist.ReadingListIngestService
import com.semweb.lsp.readinglist.ReadingList;
import grails.gorm.multitenancy.Tenants


@Slf4j
@Stepwise
@Integration
@Rollback
class LifecycleSpec extends Specification {

    final static Logger logger = LoggerFactory.getLogger(LifecycleSpec.class);

    private static String TENANT_ID='testtenant'
    
    // @Shared private GrailsApplication grailsApplication
    @Shared private static Map<String, Object> keys = null;
    @Shared private static Key key = null;
    @Shared private static String clusteradmin_jwt = null;
    @Shared private static String ittenant_jwt = null;
    @Shared Date expiration = new Date(System.currentTimeMillis()+60000);
    @Shared List<String> clusteradmin_roles = ['ROLE_clusteradmin', 'clusteradmin']
    @Shared List<String> user_roles = ['ROLE_user', 'user']
    @Shared private HazelcastInstance instance = null;
    @Shared private IMap<String, String> tenant_public_key_map = null;

    ReadingListIngestService readingListIngestService

    // Get RSA keys. Uses key size of 2048.
    private static Map<String, Object> getRSAKeys() throws Exception {
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
      keyPairGenerator.initialize(2048);
      KeyPair keyPair = keyPairGenerator.generateKeyPair();
      PrivateKey privateKey = keyPair.getPrivate();
      PublicKey publicKey = keyPair.getPublic();
      Map<String, Object> keys = new HashMap<String, Object>();
      keys.put("private", privateKey);
      keys.put("public", publicKey);
      return keys;
    }

    def validate(String jwt, String publickey_str) {

      byte[] publicBytes = Base64.getDecoder().decode(publickey_str);
      X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      PublicKey key = keyFactory.generatePublic(keySpec);

      // PublicKey publicKey = decodePublicKey(pemToDer(publickey));
      Claims claims = Jwts.parser()
           .setSigningKey(key)
           .parseClaimsJws(jwt).getBody();

      return claims
    }

    // Run before all the tests:
    def setup() {
    }
 
    // Run after all the tests, even after failures:
    def cleanupSpec() {
      println("Cleanup after all tests!")
      if ( tenant_public_key_map ) {
        tenant_public_key_map.evict("private")
        tenant_public_key_map.evict("public")
        println("tenant_public_key_map: ${tenant_public_key_map}");
      }
    }

    def setupSpec() {
      logger.info("setupSpec()");

      keys = getRSAKeys();
      key = keys.'private'

      // Create a JWT for a clusteradmin user
      clusteradmin_jwt = Jwts.builder()
          .setIssuer("me")
          .setSubject("admin")
          .setAudience("you")
          .setId(UUID.randomUUID().toString()) //just an example id
          .setIssuedAt(new Date()) // for example, now
          .claim('roles', clusteradmin_roles)
          .claim('realm_access', [ roles: clusteradmin_roles])  // Keycloak puts roles under realmaccess - emulate
          .setExpiration(expiration) //a java.util.Date
          .setNotBefore(new Date()) //a java.util.Date 
          .signWith(key)
          .compact()

      logger.info("Generated clusteradmin_jwt: ${clusteradmin_jwt}");

      ittenant_jwt = Jwts.builder()
          .setIssuer("me")
          .setSubject("admin")
          .setAudience("you")
          .setId(UUID.randomUUID().toString()) //just an example id
          .setIssuedAt(new Date()) // for example, now
          .claim('roles', user_roles)
          .claim('realm_access', [ roles: user_roles])  // Keycloak puts roles under realmaccess - emulate
          .setExpiration(expiration) //a java.util.Date
          .setNotBefore(new Date()) //a java.util.Date 
          .signWith(key)
          .compact()
      logger.info("Generated ittenant_jwt: ${ittenant_jwt}");

      def cfg = new Config('mtcp')
      instance = Hazelcast.getOrCreateHazelcastInstance(cfg)
      tenant_public_key_map = instance.getMap("realmkeys")

      // install the public key in the cache of public keys
      String public_key = Base64.getEncoder().encodeToString(keys.'public'.getEncoded())
      tenant_public_key_map.evict("private")
      tenant_public_key_map.evict("public")

      println("public key map should be empty at start: ${tenant_public_key_map}");
      tenant_public_key_map.put('clusteradmin', public_key, 600L, TimeUnit.SECONDS)
      tenant_public_key_map.put(TENANT_ID, public_key, 600L, TimeUnit.SECONDS)

    }

    def cleanup() {
    }

    void "Validate Keys"(key, expected_role) {
      when:"We see a JWT - ${key}"
        byte[] public_key_bytes = keys.'public'.getEncoded();
        String public_key_encoded = Base64.getEncoder().encodeToString(public_key_bytes)

      then:"We validate it"
        def claims = validate(key, public_key_encoded);
        logger.info("Claims: ${claims}, roles: ${claims.roles}");

      expect:"it is valid"
        claims != null
        claims.roles.contains(expected_role)
        
      where:
        key | expected_role
        clusteradmin_jwt | 'clusteradmin'
        ittenant_jwt | 'user'
    }

  void "create test tenant"() {
    when:"we call the tenantadmn endpoint with our clusteradmin jwt"
      String public_key_encoded = Base64.getEncoder().encodeToString(keys.'public'.getEncoded())

      String baseUrl = "http://localhost:$serverPort"
      println("Baseurl will be ${baseUrl}");
      HttpBuilder lsp_service = configure {
        request.uri = baseUrl
      }

      def result = lsp_service.post {
        request.uri.path = '/lspcore/tenantAdmin/registerTenant'
        request.headers['Authorization'] = "Bearer $clusteradmin_jwt".toString()
        request.headers['accept']='application/json'
        request.contentType='application/json'
        request.body = [
          tenant: TENANT_ID,
          resolvers: []
        ]

        response.when(200) { FromServer fs, Object body ->
          println("OK ${body}");
          return body
        }
        response.failure { FromServer fs, Object body ->
          println("Problem ${body} ${fs} ${fs.getStatusCode()}");
        }
      }


    then:"${TENANT_ID} is created"
      println("registerTenant reuslt: ${result}");
      1==1
  }

  void "Provision database"() {

    when:"we call the tenantadmn endpoint with our clusteradmin jwt to provision a new database"
      String baseUrl = "http://localhost:$serverPort"
      println("Baseurl will be ${baseUrl}");
      HttpBuilder lsp_service = configure {
        request.uri = baseUrl
      }

      def result = lsp_service.get {
        request.uri.path = '/lspcore/tenantAdmin/provision'
        request.headers['Authorization'] = "Bearer $clusteradmin_jwt".toString()
        request.headers['accept']='application/json'
        request.contentType='application/json'
        request.body=[
          dbname: TENANT_ID+'_db',
          user: 'ttuser',
          pass: 'ttpass'
        ]

        response.when(200) { FromServer fs, Object body ->
          println("response body: ${body}");
          return body
        }
        response.failure { FromServer fs, Object body ->
          println("Problem ${body} ${fs} ${fs.getStatusCode()}");
        }
      }

    then:"New database created"
      println("provision db Result: ${result}");
      1==1
  }

  void "Register new datasource"() {
    when:""
      String baseUrl = "http://localhost:$serverPort"
      HttpBuilder lsp_service = configure {
        request.uri = baseUrl
      }

      def result = lsp_service.get {
        request.uri.path = '/lspcore/tenantAdmin/registerDataSource'
        request.headers['Authorization'] = "Bearer $clusteradmin_jwt".toString()
        request.headers['accept']='application/json'
        request.contentType='application/json'

        request.body= [
          dscode:TENANT_ID+'_ds',
          driverClassName: 'org.postgresql.Driver',
          username: 'ttuser',
          password: 'ttpass',
          dbcreate: 'none',
          dbname: TENANT_ID+'_db',
          dbhost: 'localhost',   // may need to parameterise this
          dialect: 'org.hibernate.dialect.PostgreSQL94Dialect'
        ]

        response.when(200) { FromServer fs, Object body ->
          println("registerNewDataSource result: ${body}");
          return body;
        }
        response.failure { FromServer fs, Object body ->
          println("registerNewDataSource Problem ${body} ${fs} ${fs.getStatusCode()}");
        }
      }

    then:"The datasource is registered"
      println("Result of registerNewDataSource: ${result}");
      1==1
  }

  void "link Service To Data Source"() {
    when:""
      String baseUrl = "http://localhost:$serverPort"
      HttpBuilder lsp_service = configure {
        request.uri = baseUrl
      }

      def result = lsp_service.get {
        request.uri.path = '/lspcore/tenantAdmin/registerTSVDS'
        request.headers['Authorization'] = "Bearer $clusteradmin_jwt".toString()
        request.headers['accept']='application/json'
        request.contentType='application/json'

        request.body= [
          dscode:TENANT_ID+'_ds',
          service: 'lsp_core',
          version: '*',
          tenant: TENANT_ID
        ]

        response.when(200) { FromServer fs, Object body ->
          println("registerNewDataSource result: ${body}");
          return body;
        }
        response.failure { FromServer fs, Object body ->
          println("registerNewDataSource Problem ${body} ${fs} ${fs.getStatusCode()}");
        }
      }

    then:"The datasource is registered"
      println("Result of registerNewDataSource: ${result}");
      1==1
  }

  // println("Installing app against datasource");
  // installOrUpgrade(mt_context,
  //                  mtservice,
  //                  clusteradmin_jwt,
  //                  new_tenant,
  //                  [ admuser:[ username:'admin' ], rootOU:[ code:'TestOrg', name:'My test Org' ] ] );
  void "Install or upgrade service for tenant"() {
    when:""
      String baseUrl = "http://localhost:$serverPort"
      HttpBuilder lsp_service = configure {
        request.uri = baseUrl
      }

      def result = lsp_service.get {
        request.uri.path = '/lspcore/tenantAdmin/installOrUpgrade'
        request.headers['Authorization'] = "Bearer $clusteradmin_jwt".toString()
        request.headers['accept']='application/json'
        request.contentType='application/json'

        request.uri.query= [
          tenant: TENANT_ID
        ]

        response.when(200) { FromServer fs, Object body ->
          println("installOrUpgrade result: ${body}");
          return body;
        }
        response.failure { FromServer fs, Object body ->
          println("installOrUpgrade Problem ${body} ${fs} ${fs.getStatusCode()}");
        }
      }

    then:"The datasource is registered"
      println("Result of registerNewDataSource: ${result}");
      1==1
  }

  void "Import reading list"(filename, num) {
    when: "We import a reading list"
      Tenants.withId(TENANT_ID) {
        ReadingList.withNewTransaction { status ->
          logger.debug("Import reading list: ${filename}");
          InputStreamReader isr = new InputStreamReader( getClass().getResourceAsStream(filename) )
          ReadingList rl = readingListIngestService.process(isr)
        }
      }

    then: "Reading list is imported"
      1==1

    where:
      filename | num
      '/testdata/readinglist/rl1.tsv' | 0
  }
}
