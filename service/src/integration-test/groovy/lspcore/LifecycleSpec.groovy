package lspcore


import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import java.security.Key;
import java.security.PublicKey
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import spock.lang.Shared
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import groovy.util.logging.Slf4j
import spock.lang.Stepwise
import com.hazelcast.config.Config
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.map.IMap
import java.util.concurrent.TimeUnit
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer
import static groovyx.net.http.HttpBuilder.configure
import java.io.InputStreamReader
import lspcore.readinglist.ReadingListIngestService
import com.semweb.lsp.readinglist.ReadingList;
import grails.gorm.multitenancy.Tenants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value


@Slf4j
@Stepwise
@Integration
@Rollback
class LifecycleSpec extends Specification {

  final static Logger logger = LoggerFactory.getLogger(LifecycleSpec.class);

  final static String CORE_SERVICE_NAME='holocoreservice'
  final static String LSP_SERVICE_NAME='lsp_core'


  def tenantConfigService
  def hibernateDatastore

  @Value('${local.server.port}')
  Integer serverPort

  @Shared
  public static Integer static_server_port = null;

  // Run before all the tests:
  def setup() {
    static_server_port = serverPort
  }
 
  // Run after all the tests, even after failures:
  def cleanupSpec() {
  }

  def setupSpec() {
    logger.info("setupSpec()");
  }

  def cleanup() {
  }





 void "Configure MTCPAdm"() {
    when: "We log on to the master KC realm"
      logger.debug("Attempt login");
      String jwt = keycloakLogin('master', 'admin-cli', 'admin', 'admin');
      logger.debug("Master realm admin jwt: ${jwt}");

    then: "We call the configure service"
      String register_tenant_status = null;
      def httpBin = HttpBuilder.configure {
        request.uri = 'http://localhost:8080'
      }

      def result = httpBin.post {
        request.uri.path = '/mtcpmgt/configure'
        request.headers['accept'] = 'application/json'
        request.contentType='application/json'
        request.headers['Authorization'] = "bearer ${jwt}"
        request.body = [
          kc_admin:'admin',
          kc_pass:'admin'
        ]

        response.when(200) { FromServer fs, Object body ->
          register_tenant_status='OK'
          logger.debug("MTCPMgt configured");
        }

        response.failure { FromServer fs, Object body ->
          register_tenant_status='ERROR'
          logger.debug("/mtcpmgt/configure returns 400 ${body} register_tenant_status=${register_tenant_status}");
        }

      }

    then: "All is well"
      register_tenant_status == 'OK'
  }


  /**
   *  Step 1 - register some tenants - this will provision a default database for the tenant and perform related setup
   */
  void "register a tenant with a tenant admin web API"(tenant, resolvercfg) {
    when: "We register a tenant with a resolver via the web API"
      logger.debug("RegisterTenant via Web API ${tenant}/${resolvercfg}");
      String jwt = keycloakLogin('master','admin-cli','admin','admin');
      String register_tenant_status = null;
      def httpBin = HttpBuilder.configure {
        request.uri = 'http://localhost:8080'
      }

      def result = httpBin.post {
        request.uri.path = '/mtcpmgt/registerTenant'
        request.headers['accept'] = 'application/json'
        request.contentType='application/json'
        request.headers['Authorization'] = "bearer ${jwt}"
        request.body = [
          tenant: tenant,
          resolvers: resolvercfg,
          tenantData: [ users: [ [username:'usr', email:'user@a.b.c', firstName:'usera', lastName:'userb', groups:['users','admins'], roleS:[], password:'usrpass' ] ] ]
        ]

        response.when(200) { FromServer fs, Object body ->
          register_tenant_status='OK'
          logger.debug("Server should return 200 when adding new tenant : ${body} (register_tenant_status=${register_tenant_status})")
        }

        response.failure { FromServer fs, Object body ->
          register_tenant_status='ERROR'
          logger.debug("/registerTenant returns 400 ${body} register_tenant_status=${register_tenant_status}");
        }

      }

    then: "All is well"
      register_tenant_status == 'OK'

    where:
      tenant|resolvercfg
      'TestTenantG'| [ [ method:'Host', content:'testtenantg.some.domain.net' ], [ method:'Host', content:'ttg.other.domain.net' ] ]
      'TestTenantH'| null
      'TestTenantJ'| [ [ method:'Host', content:'testtenantj.some.domain.net' ], [ method:'Host', content:'ttj.other.domain.net' ] ]
  }


  /**
   *  Step 2 - register some services 
   */
  void "register a service via MTCP web API"(service, service_descriptor) {

    when: "We register a service with the web API"
      logger.debug("RegisterService via Web API ${service}/${service_descriptor}");
      String jwt = keycloakLogin('master','admin-cli','admin','admin');
      String register_service_status = null;
      def httpBin = HttpBuilder.configure {
        request.uri = 'http://localhost:8080'
      }

      def result = httpBin.post {
        request.uri.path = '/mtcpmgt/registerService'
        request.headers['accept'] = 'application/json'
        request.contentType='application/json'
        request.headers['Authorization'] = "bearer ${jwt}"
        request.body = service_descriptor

        response.when(200) { FromServer fs, Object body ->
          register_service_status='OK'
          logger.debug("Server should return 200 when adding new service : ${body} (register_service_status=${register_service_status})")
        }

        response.failure { FromServer fs, Object body ->
          register_service_status='ERROR'
          logger.debug("/registerService failure ${body} register_service_status=${register_service_status}");
        }

      }

    then: "All is well"
      register_service_status == 'OK'

    // host.docker.internal / 172.17.0.1
    where:
      service|service_descriptor
      CORE_SERVICE_NAME|[ service_name: CORE_SERVICE_NAME, service_addr: "http://localhost:8080", enable_path:'/holoCoreService/tenantAdmin/installOrUpgrade', per_tenant_config: [ 'dataSource':[ 'type':'dataSource' ] ] ]
      LSP_SERVICE_NAME|[ service_name: LSP_SERVICE_NAME, service_addr: "http://localhost:${static_server_port}", enable_path:'/holoCoreService/tenantAdmin/installOrUpgrade', per_tenant_config: [ 'dataSource':[ 'type':'dataSource' ] ] ]
  }



  /**
   *  Step 3 - Enable services for tenant
   */
  void "enable a service via MTCP web API"(service, tenant) {

    when: "We register a service with the web API"
      logger.debug("enable via Web API ${service}/${tenant}");
      String jwt = keycloakLogin('master','admin-cli','admin','admin');
      String register_service_status = null;
      def httpBin = HttpBuilder.configure {
        request.uri = 'http://localhost:8080'
      }

      def result = httpBin.get {
        request.uri.path = '/mtcpmgt/enableServiceForTenant'
        request.headers['accept'] = 'application/json'
        request.contentType='application/json'
        request.headers['Authorization'] = "bearer ${jwt}"
        request.uri.query = [
          tenant: tenant,
          service: service
        ]

        response.when(200) { FromServer fs, Object body ->
          register_service_status='OK'
          logger.debug("Server should return 200 when adding new service : ${body} (register_service_status=${register_service_status})")
        }

        response.failure { FromServer fs, Object body ->
          register_service_status='ERROR'
          logger.debug("/mtcpmgt/enableServiceForTenant failure ${body} register_service_status=${register_service_status}");
        }

      }

    then: "All is well"
      register_service_status == 'OK'

    where:
      service|tenant
      CORE_SERVICE_NAME|'TestTenantG'
      LSP_SERVICE_NAME|'TestTenantG'
  }



  private String keycloakLogin(String realm, String client_id, String user, String password) {

    String result = null;

    def keycloak = configure {
      request.uri = 'http://localhost:8080'
    }

    // Log on to the keycloak master realm with the master password
    def r = keycloak.post {
        request.uri.path = "/auth/realms/${realm}/protocol/openid-connect/token".toString()
        request.contentType='application/x-www-form-urlencoded'
        request.body = [
          grant_type:'password',
          client_id:client_id, // 'holo',  // This should be 'holo' rather than 'admin-cli' for the full list of attrs?
          username:user,
          password:password
        ]
        response.when(200) { FromServer fs, Object body ->
          result = body.access_token
        }
        response.failure { FromServer fs, Object body ->
          logger.error("Problem ${body} ${fs} ${fs.getStatusCode()}");
        }
    }

    return result;
  }

}
