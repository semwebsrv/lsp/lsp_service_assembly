package com.semweb.csv;

import com.opencsv.CSVReader
import com.opencsv.CSVReaderBuilder


/**
 * CSVHelper is a class that works on csv or tsv files that are structured as a file containing a header line followed by
 * 0 or more data lines. Such files are a challenge to work with because column orders can be moved around, extra columns can be
 * inserted, and local profiles can have synonyms for column names as well as variants for case. The helper is passed a map containing
 * column definitions and order and the job if eachRow is to parse the header and turn each row into a canonical map which is then
 * passed to the closure for handling by the caller. This provides a mechanism whereby many different variants of a cvs header can be
 * processed using a single mechanism.
 * @param config a map containing the following
 *
 *   columns: [
 *     [ 'canonicalName': 'xxx', patterns:['a','b','c'] ]
 *   ],
 *   unmappedColumnAction:Ignore|Passthough|other
 *
 *
 * @param csvReader
 * @param c
 
 */
public class CSVHelper {

  private static List processHeader(String[] csv_row, Map config) {
    List result = []

    csv_row.each { column_header ->
      // look up column_header in 
      String colname = column_header.trim().toLowerCase()

      // replace ( v.patterns?.contains(colname) ) ) }
      // with v.patterns.find { pattern -> colname ==~ pattern }

      // Find any mapping where the canonical name is an exact match or where the regex pattern matches
      Map mapping = config.columns.find { v -> ( ( v.canonicalName==colname ) || 
                                                 ( v.patterns?.find { pattern -> colname ==~ pattern } ) ) }
      if ( mapping ) {
        if ( mapping.handler != null ) {
          result.add ( [ action:3, colname:colname, handler: mapping.handler ] )
        }
        else {
          result.add ( [ action:1, field: mapping.canonicalName, handler: mapping.handler ] )
        }
      }
      else {
        if ( config.unmappedColumnAction == 'passthrough' ) {
          result.add ( [ action: 1, field: column_header ] )  // 1- add field to map
        }
        else if ( config.unmappedColumnAction == 'other' ) {
          result.add ( [ action: 2, field: column_header ] ) // 2 - add field to __otherFields collection
        }
      }
    }
    return result;
  }

  private static Map processRow(String[] row, List column_mappings) {
    Map result = [:]
    int i=0;
    column_mappings.each { coldef ->
      if ( i < row.length ) {
        // If the action is add another key to the row map, do so
        if( coldef.action==1 ) {
          result[coldef.field] = row[i]
        }
        else if ( coldef.action==2 ) {  // If the action is to add the field to the __otherFields bag, do so
          if ( result['__otherFields'] == null )
            result['__otherFields'] = [:]
 
          if ( result['__otherFields'][coldef.field] != null ) {
            result['__otherFields'][coldef.field].add(row[i])
          }
          else {
            result['__otherFields'][coldef.field] =  [ row[i] ]
          }
        }
        else if ( coldef.action==3 ) {
          // mapping contains a closure we should call
         
          coldef.handler.call(coldef.colname, result, row[i]);
        }
        else {
          log.error("Un handled column definition action");
        }
      }
      i++
    }
    return result;
  }

  public static void eachRow(Map config, CSVReader csvReader, Closure c) {
    String[] csv_row = null;

    // Read the header row
    csv_row = csvReader.readNext()

    List column_mappings = processHeader(csv_row, config);

    while ((csv_row = csvReader.readNext()) != null) {
      // log.debug("Process csv row ${csv_row}");
      Map object_for_this_row = processRow(csv_row,column_mappings)
      c.call( object_for_this_row )
    }
  }
}
