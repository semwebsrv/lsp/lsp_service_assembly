buildscript {
    repositories {
        maven { url "https://repo.grails.org/grails/core" }
    }
    dependencies {
        classpath "org.grails:grails-gradle-plugin:$grailsVersion"
        classpath "gradle.plugin.com.github.erdi.webdriver-binaries:webdriver-binaries-gradle-plugin:2.0"
        classpath "org.grails.plugins:hibernate5:7.0.0"
        classpath "com.bertramlabs.plugins:asset-pipeline-gradle:3.0.10"
        classpath 'org.grails.plugins:database-migration:3.1.0.RC1'
        classpath "org.grails.plugins:views-gradle:2.0.0.RC2"
    }
}

configurations.all {
    // Check for updates every build
    resolutionStrategy.cacheChangingModulesFor 120, 'seconds'
}

version "${appVersion}"
group "com.semweb.lsp"

apply plugin:"eclipse"
apply plugin:"idea"
apply plugin:"war"
apply plugin:"org.grails.grails-web"
apply plugin:"org.grails.plugins.views-json"

sourceSets {
    main {
        resources {
            srcDir 'grails-app/migrations'
        }
    }
}

repositories {
    maven { url "https://nexus.semweb.co/repository/maven-public" }
    maven { url "https://repo.grails.org/grails/core" }
    mavenLocal()
    maven { url "https://nexus.semweb.co/repository/maven-public" }
    maven { url "https://oss.jfrog.org/artifactory/oss-snapshot-local" }
}

configurations {
    developmentOnly
    runtimeClasspath {
        extendsFrom developmentOnly
    }
}

dependencies {
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    compile "org.springframework.boot:spring-boot-starter-logging"
    compile "org.springframework.boot:spring-boot-autoconfigure"
    compile "org.grails:grails-core"
    compile "org.springframework.boot:spring-boot-starter-actuator"
    compile "org.springframework.boot:spring-boot-starter-tomcat"
    compile "org.grails:grails-plugin-url-mappings"
    compile "org.grails:grails-plugin-rest"
    compile "org.grails:grails-plugin-codecs"
    compile "org.grails:grails-plugin-interceptors"
    compile "org.grails:grails-plugin-services"
    compile "org.grails:grails-plugin-datasource"
    compile "org.grails:grails-plugin-databinding"
    compile "org.grails:grails-web-boot"
    compile "org.grails:grails-logging"
    compile "org.grails.plugins:cache"
    compile "org.grails.plugins:async"
    compile "org.grails.plugins:events:4.0.0"
    compile "org.grails.plugins:views-json"
    compile "org.grails.plugins:views-json-templates"
    compile "org.postgresql:postgresql:42.2.8"
    compile "org.grails.plugins:database-migration:3.1.0.RC1"
    compile "org.liquibase:liquibase-core:3.8.4"
    compile "org.grails.plugins:hibernate5"
    compile "org.hibernate:hibernate-core:5.4.0.Final"
    compile "io.github.http-builder-ng:http-builder-ng-core:1.0.4"
    compile "com.h2database:h2"
    compileOnly "io.micronaut:micronaut-inject-groovy"
    console "org.grails:grails-console"
    testCompile "org.grails:grails-gorm-testing-support"
    testCompile "org.mockito:mockito-core"
    testCompile "io.micronaut:micronaut-http-client"
    testCompile "org.grails:grails-web-testing-support"

    profile "com.k-int.grails.profiles:mtapi:1.0.0-SNAPSHOT"

    compile 'com.semweb.mtcontrolplane:MTControlPlane:2.0.4'
    compile 'com.semweb.holo:holocore:1.1.9'
    compile 'com.semweb:graphqlmt:0.1.12'
    compile "com.semweb:gormwherelucenequery:0.1.6"

    compile('com.semweb.lsp:lsp_core_domain_model:1.0.5')

    // These would normally come transitively via plugin dependencies but we don't currently
    // have proper pom files for those, so add the deps manually here
    compile 'com.graphql-java:graphql-java:13.0'
    compile 'org.apache.lucene:lucene-queryparser:8.4.0'
    compile 'org.apache.lucene:lucene-analyzers-common:8.4.0'

  

    // Additional deps
    compile 'org.grails.plugins:spring-security-core:4.0.0'
    compile 'org.springframework.security:spring-security-openid:5.1.6.RELEASE'
    compile 'com.ibm.icu:icu4j:62.1'
    compile 'org.apache.commons:commons-lang3:3.8.1'
    runtime "org.slf4j:jul-to-slf4j:1.7.7"
    runtime "org.slf4j:log4j-over-slf4j:1.7.7"

    // Suspect spring-security-core is dragging this dependency down to 3.x - explicitly bump it
    compile 'com.hazelcast:hazelcast:4.0.1'
    compile 'io.jsonwebtoken:jjwt-impl:0.11.0'
    compile 'io.jsonwebtoken:jjwt-jackson:0.11.0'
 
    compile 'com.opencsv:opencsv:5.2'
    compile 'commons-io:commons-io:2.7'

}

bootRun {
    ignoreExitValue true
    jvmArgs(
        '-Dspring.output.ansi.enabled=always', 
        '-noverify', 
        '-XX:TieredStopAtLevel=1',
        '-Xmx1024m')
    sourceResources sourceSets.main
    String springProfilesActive = 'spring.profiles.active'
    systemProperty springProfilesActive, System.getProperty(springProfilesActive)
}

tasks.withType(GroovyCompile) {
    configure(groovyOptions) {
        forkOptions.jvmArgs = ['-Xmx1024m']
    }
}


