package lspcore

class BootStrap {

  def grailsApplication

  def init = { servletContext ->
    String app_version = grailsApplication.metadata.getApplicationVersion();
    String app_name = grailsApplication.metadata.getApplicationName();

    log.info("**lsp-core-service**");
    log.info("app_name=${app_name}, app_version=${app_version}");
    log.info("              keycloak -> ${grailsApplication.config.keycloakUrl}");
    log.info("          build number -> ${grailsApplication.metadata['build.number']}");
    log.info("        build revision -> ${grailsApplication.metadata['build.git.revision']}");
    log.info("          build branch -> ${grailsApplication.metadata['build.git.branch']}");
    log.info("          build commit -> ${grailsApplication.metadata['build.git.commit']}");
    log.info("            build time -> ${grailsApplication.metadata['build.time']}");
  }

  def destroy = {
  }

}
