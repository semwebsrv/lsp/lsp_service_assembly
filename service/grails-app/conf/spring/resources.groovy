// Place your Spring DSL code here
import lspcore.LSPUserDetailsService
import holo.core.user.UserPasswordEncoderListener
import lspcore.LSPGraphqlContextBuilder

beans = {
  userPasswordEncoderListener(UserPasswordEncoderListener)
  userDetailsService(LSPUserDetailsService)
  graphqlContextBuilder(LSPGraphqlContextBuilder)
}
