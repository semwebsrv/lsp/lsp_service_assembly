package lspcore

import grails.core.GrailsApplication
import grails.plugins.*
import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import grails.gorm.multitenancy.CurrentTenant
import lspcore.readinglist.ReadingListIngestService
import com.semweb.lsp.readinglist.ReadingList;
import org.apache.commons.io.input.BOMInputStream
import lspcore.readinglist.ReadingListIngestService
import javax.servlet.http.Part
import com.semweb.lsp.readinglist.ReadingList


@CurrentTenant
class ReadingListsController implements PluginManagerAware {

  GrailsApplication grailsApplication
  GrailsPluginManager pluginManager
  ReadingListIngestService readingListIngestService

  @Secured('ROLE_user')
  def upload() {
    log.debug("ReadingListController::upload ${params}");

    Part p = request.getPart('file')
    if ( p ) {
      log.debug("Processing attached file");
      BOMInputStream bis = new BOMInputStream(p.getInputStream());
      Reader fr = new InputStreamReader(bis);
      ReadingList.withTransaction { status ->
        readingListIngestService.process(fr);
      }
    }
    else {
      log.debug("No attached multipart field named \"file\"");
    }
  

    Map result = [
      'app': 'lsp_core'
    ]

    render result as JSON
  }
}

