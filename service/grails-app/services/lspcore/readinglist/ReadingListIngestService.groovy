package lspcore.readinglist

import com.semweb.lsp.readinglist.ReadingList;
import com.opencsv.CSVReader
import com.opencsv.CSVReaderBuilder
import com.opencsv.CSVParserBuilder
import com.opencsv.CSVParser
import com.semweb.csv.CSVHelper
import com.semweb.lsp.kb.ResolverService
import com.semweb.lsp.kb.IngestService
import com.semweb.lsp.kb.Work;
import com.semweb.lsp.kb.Instance;


public class ReadingListIngestService {

  ResolverService resolverService
  IngestService ingestService

  private static Map rl_tsv_config = [
    columns: [
      [ 'canonicalName': 'module_code', patterns:['module code'] ],
      [ 'canonicalName': 'rl_title', patterns:['reading list title'] ],
      [ 'canonicalName': 'order_line_note', patterns:['for order line note'] ],
      [ 'canonicalName': 'number_of_students', patterns:['number of students'] ],
      [ 'canonicalName': 'fund_code', patterns:['fund code'] ],
      [ 'canonicalName': 'location_code', patterns:['location code'] ],
      [ 'canonicalName': 'library_code', patterns:['library code'] ],
      [ 'canonicalName': 'library_name', patterns:['library name'] ],
      [ 'canonicalName': 'exceptions', patterns:['exceptions'] ],
      [ 'canonicalName': 'prior_review', patterns:['potentially previously reviewed'] ],
      [ 'canonicalName': 'priority', patterns:['book priority'] ],
      [ 'canonicalName': 'suggested_qry', patterns:['suggested number for reading list'] ],
      [ 'canonicalName': 'total_held', patterns:['total number of copies held'] ],
      [ 'canonicalName': 'title', patterns:['book title'] ],
      [ 'canonicalName': 'isbn', patterns:['book isbn'] ],
      [ 'canonicalName': 'isbn-13', patterns:['book isbn-13'] ],
      [ 'canonicalName': 'lcn', patterns:['Book lcn'] ],
      [ 'canonicalName': 'dup_match', patterns:['duplication match (added by gobi)'] ],
      [ 'canonicalName': 'eavail', patterns:['eavailability (added by gobi)'] ],
      [ 'canonicalName': 'dda_eligible', patterns:['dda eligibility (added by gobi)'] ],
      [ 'canonicalName': 'newer_edition', patterns:['newer edition available (added by gobi)'] ],
      [ 'canonicalName': 'qty_p', patterns:['quantity p'] ],
      [ 'canonicalName': 'qty_e', patterns:['quantity e'] ],
      [ 'canonicalName': 'price_p', patterns:['price p '] ],
      [ 'canonicalName': 'price_e', patterns:['price e'] ],
      [ 'canonicalName': 'library_note', patterns:['book - note for library'] ],
      [ 'canonicalName': 'student_note', patterns:['book - note for student'] ],
      [ 'canonicalName': 'edition', patterns:['edition'] ],
      [ 'canonicalName': 'pubdate', patterns:['publication date'] ],
      [ 'canonicalName': 'total_no_items', patterns:['total Number of items'] ],
      [ 'canonicalName': 'total_loc', patterns:['no. Items in .*'], handler:{ colname, result, value -> String loc = colname.subString(13); println("loc : ${loc}"); } ],
      [ 'canonicalName': 'item_link', patterns:['item link'] ]
    ],
    unmappedColumnAction:'other'
  ];

  public ReadingList process(Reader r) {

    CSVParser parser = new CSVParserBuilder()
                                    .withSeparator('\t' as char)
                                    .build();

    CSVReader csvReader = new CSVReaderBuilder(r)
                                 .withCSVParser(parser)
                                 .build();

    CSVHelper.eachRow(rl_tsv_config, csvReader) { rl_entry ->
      log.debug("Process reading list entry ${rl_entry}");

      List identifiers = []
      identifiers.addAll( rl_entry.isbn?.split(',').collect { [ authority:'isbn', value:it.trim() ] } )
      identifiers.addAll( rl_entry.'isbn-13'?.split(',').collect { [ authority:'isbn', value:it.trim() ] } )

      Map description = [
        title: rl_entry.rl_title,
        identifiers: identifiers
      ]

      log.debug("Attempt match ${description}");
      def match_result = resolverService.bestEffortsMatch(description)
      
      if ( ( match_result.possibleInstances.size() == 0 ) && ( match_result.possibleWorks.size() == 0 ) ) {
        resolverService.absorb(description)
      }
    }

  }

}

