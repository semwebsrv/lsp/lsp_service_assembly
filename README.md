# HOLO LSP Service Assembly


## Code-Build-Run cycle for the lspcore service

lspcore is a module that is built atop the holo framework, that means that a number of dependent services must be started first.

In order to enable a code-build-run loop, the docker-compose.yml file is provided at the project root - this provides all dependent
services. The steps needed are

0. docker-compose pull - make sure you have the latest docker images
1. docker-compose up -- Start the core services
2. source ./env.sh -- Make sure you are running the right versions of java and groovy
3. cd service -- Build lspcore in the service directory
4. ./gradlew build -- Build a runtime executable war
5. cd ../scripts -- We will run the executable script from this dir using
6. ./run_lsp_service.sh - start lspcore war in the holo network using the dependencies from docker-compose.yml
7. Set up your default environment - create a new shell in the scripts directory and run
8. ./setup_2.sh - to configure a tenant, register modules, and enable modules for the tenant

Make changes to the code, rebuild, docker-kill lspcore and then re-launch with step 6 - thats your main code-build-run loop.



# Notes
https://grails-plugins.github.io/grails-spring-security-core/2.0.x/guide/userDetailsService.html
https://github.com/alvarosanchez/grails-spring-security-rest/blob/ffa848c9c6dd82f92f2ab489cb5d7a1515c587f2/spring-security-rest/src/main/groovy/grails/plugin/springsecurity/rest/RestAuthenticationProvider.groovy

If you're dynamically changing domain models, 

    ./gradlew build --refresh-dependencies

can be used to force downloads of new snapshots


