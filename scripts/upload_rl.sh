#!/bin/bash

BASE_URL="http://localhost:8080"

LSP_USER="usr"
LSP_PASS="usrpass"
CLIENT="holo"
KEYCLOAK_REALM="holo1"


# -H 'accept: application/json' -H 'Content-type: application/x-www-form-urlencoded' \
KEYCLOAK_RESPONSE=$(curl -s -X POST -H 'Content-type: application/x-www-form-urlencoded' \
        --data-urlencode "client_id=$CLIENT" \
        --data-urlencode "username=$LSP_USER" \
        --data-urlencode "password=$LSP_PASS" \
        --data-urlencode "grant_type=password" \
        "${BASE_URL}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/token")

echo response: $KEYCLOAK_RESPONSE


USR_JWT=`echo $KEYCLOAK_RESPONSE | jq -rc '.access_token'`

echo Master JWT will be $USR_JWT
printf "\n\nUpload readinglist\n"
curl -H "Authorization: bearer $USR_JWT" \
     -H "X-TENANT: holo1" \
     -F "file=@../testdata/rl1.tsv" \
     -X POST $BASE_URL/lspcore/readingLists/upload
