#!/bin/bash

# CLUSTERADMIN_USER="clusteradm_usr"
# CLUSTERADMIN_PASS="m89JD&&TA%Bx"
# KEYCLOAK_CLIENT="holo"
# KEYCLOAK_REALM="clusteradmin"
BASE_URL="http://localhost:8080"

CLUSTERADMIN_USER="admin"
CLUSTERADMIN_PASS="admin"
KEYCLOAK_CLIENT="admin-cli"
KEYCLOAK_REALM="master"


# -H 'accept: application/json' -H 'Content-type: application/x-www-form-urlencoded' \
KEYCLOAK_MASTER_RESPONSE=$(curl -s -X POST -H 'Content-type: application/x-www-form-urlencoded' \
        --data-urlencode "client_id=$KEYCLOAK_CLIENT" \
        --data-urlencode "username=$CLUSTERADMIN_USER" \
        --data-urlencode "password=$CLUSTERADMIN_PASS" \
        --data-urlencode "grant_type=password" \
        "${BASE_URL}/auth/realms/${KEYCLOAK_REALM}/protocol/openid-connect/token")


KEYCLOAK_MASTER_JWT=`echo $KEYCLOAK_MASTER_RESPONSE | jq -rc '.access_token'`

echo Master JWT will be $KEYCLOAK_MASTER_JWT


printf "\n\nConfigure mtcp\n"
curl -H "Authorization: bearer $KEYCLOAK_MASTER_JWT" \
     -H "Content-Type: application/json" \
     -X POST $BASE_URL/mtcpmgt/configure -d '
{
   kc_admin:"admin",
   kc_pass:"admin"
}
'


printf "\n\nRegister HoloCore Service\n"
curl -H "Authorization: bearer $KEYCLOAK_MASTER_JWT" \
     -H "Content-Type: application/json" \
     -X POST $BASE_URL/mtcpmgt/registerService -d '
{
   service_name: "holocoreservice",
   service_addr: "http://holocoreservice:8080",
   enable_path: "/holoCoreService/tenantAdmin/installOrUpgrade",
   per_tenant_config: {
     dataSource:{
       type:"dataSource"
     }
   }
}
'

printf "\n\nRegister LSP Service\n"
curl -H "Authorization: bearer $KEYCLOAK_MASTER_JWT" \
     -H "Content-Type: application/json" \
     -X POST $BASE_URL/mtcpmgt/registerService -d '
{
   service_name: "lsp_core",
   service_addr: "http://lspcore:8080",
   enable_path: "/lspcore/tenantAdmin/installOrUpgrade",
   per_tenant_config: {
     dataSource:{
       type:"dataSource"
     }
   }
}
'

printf "\n\nregister tenant\n"
curl -H "Authorization: bearer $KEYCLOAK_MASTER_JWT" \
     -H "Content-Type: application/json" \
     -X POST $BASE_URL/mtcpmgt/registerTenant -d '
{
  tenant: "holo1",
  resolvers: [
    { method: "Host", "content": "localhost" }
  ],
  tenantData: { 
    users: [ 
      {
        username:"usr", 
        email:"user@a.b.c", 
        firstName:"usera", 
        lastName:"userb", 
        groups:["users","admins"], 
        roleS:[], 
        password:"usrpass" 
      } 
    ]
  }
}
'

printf "\n\nEnable holocoreservice for holo1\n"

curl -H "Authorization: bearer $KEYCLOAK_MASTER_JWT" \
     -H "Content-Type: application/json" \
     "$BASE_URL/mtcpmgt/enableServiceForTenant?tenant=holo1&service=holocoreservice"

printf "\n\nEnable lspcore for holo1\n"

curl -H "Authorization: bearer $KEYCLOAK_MASTER_JWT" \
     -H "Content-Type: application/json" \
     "$BASE_URL/mtcpmgt/enableServiceForTenant?tenant=holo1&service=lsp_core"


printf "\n\nRequesting status\n"
STATUS=`curl -s -H "Authorization: bearer $KEYCLOAK_MASTER_JWT" "$BASE_URL/mtcpmgt/status"`
echo $STATUS | jq

