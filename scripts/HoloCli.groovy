import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer
import static groovyx.net.http.HttpBuilder.configure

public class HoloCli {

  private String baseUrl = null;
  private String token = null;
  private HttpBuilder http_client = null;

  public HoloCli(String baseUrl) {
    this.baseUrl = baseUrl;
    http_client = configure {
      request.uri = baseUrl;
    }
  }

  public static HoloCli newSession(String baseUrl, String tenant, String user, String pass) {
    HoloCli result = new HoloCli(baseUrl)
    result.login(tenant,user,pass);
    return result;
  }

  public String login(String tenant, String user, String pass) {
    this.token = login(http_client,'holo',tenant,user,pass);
    return this.token;
  }

  /**
   * Get a token from keycloak
   * login(keycloak, 'holo', new_tenant, 'admin', 'password')
   */
  private String login(HttpBuilder keycloak, 
                       String client, 
                       String tenant_as_realm, 
                       String username, 
                       String password) {

    String result = null;

    def http_res = keycloak.post {
      request.uri.path = "/auth/realms/${tenant_as_realm}/protocol/openid-connect/token".toString()
      request.contentType='application/x-www-form-urlencoded'
      request.body=[
        client_id:client,
        username:username,
        password:password,
        grant_type:'password'
      ]
  
      response.when(200) { FromServer fs, Object body ->
        // println("OK ${body}");
        result = body.access_token
      }
      response.failure { FromServer fs, Object body ->
        println("Problem ${body} ${fs} ${fs.getStatusCode()}");
      }
    }
    return result;
  }

}
