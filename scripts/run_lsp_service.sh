#!/bin/bash

the_war_file=`ls ../service/build/libs/lsp_core*war | tail -n 1`


echo Located war file $the_war_file
echo run cp "$the_war_file" "./lspservice.war"

cp "$the_war_file" "./lspservice.war"


docker run -it --rm --hostname=lspcore \
  --name=lspcore --network=lsp_service_assembly_holo \
  -v `pwd`/lspservice.war:/lspservice.war \
  -e KEYCLOAK_URL=http://lspkeycloak:8080 \
  -e MTCPMGT_URL=http://mtcpmgt:8080 \
  adoptopenjdk:11-jdk-openj9 sh -c 'java -jar /lspservice.war'
